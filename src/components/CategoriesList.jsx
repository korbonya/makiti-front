import React, {useState,useEffect} from "react";
import MegaMenu from "./mega-menu/MegaMenu"
import { motion } from "framer-motion";

const CategoriesList = ({ categories }) => {
	const [data,setData]=useState([]);
	// const [style, setStyle]=useState({display: "none"})
	const getData=()=>{
	  fetch('./categories-fr.json'
	  ,{
		headers : { 
		  'Content-Type': 'application/json',
		  'Accept': 'application/json'
		 }
	  }
	  )
		.then(function(response){
		  console.log(response)
		  return response.json();
		})
		.then(function(myJson) {
		  console.log(myJson);
		  setData(myJson)
		});
	}
	useEffect(()=>{
	  getData()
	},[])
	

	
	// const data = [
	// 	{
	// 		img: category14,
	// 		title: "Electroniques et Informatique",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category1,
	// 		title: "Transport",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category2,
	// 		title: "Immobiliers",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category4,
	// 		title: "Vêtements",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category5,
	// 		title: "Aliments",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category6,
	// 		title: "Electromenagers",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category7,
	// 		title: "Emplois",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category8,
	// 		title: "Services",
	// 		desc: "10.6K Annonces",
	// 	},
	// 	{
	// 		img: category9,
	// 		title: "Divers",
	// 		desc: "10.6K Annonces",
	// 	},
	// ];

	// const data2 = [
	// 	{
	// 		img: category10,
	// 		title: "Smartphones, Tablettes, Objets Intelligents",
	// 		desc: "7 586 Annonces",
	// 	},
	// 	{
	// 		img: category11,
	// 		title: "Ordinateurs",
	// 		desc: "2 566 Annonces",
	// 	},
	// 	{
	// 		img: category12,
	// 		title: "Multimedia",
	// 		desc: "1 200 Annonces",
	// 	},
	// 	{
	// 		img: category13,
	// 		title: "Jeux Vidéo",
	// 		desc: "150 Annonces",
	// 	},
	// ];

	// const data3 = [
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Téléphones Portables",
	// 		desc: "5 400 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Tablettes",
	// 		desc: "1 056 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Montres et Bracelets Intelligents",
	// 		desc: "562 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Pieces détachées de téléphones",
	// 		desc: "145 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Housses",
	// 		desc: "278 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Objets connectés",
	// 		desc: "146 Annonces",
	// 	},
	// 	{
	// 		img: placeHolderCategory,
	// 		title: "Accessoires",
	// 		desc: "56 Annonces",
	// 	},
	// ];
	// const [disp, setDisp]=useState(false)
	// const subDiplay=()=>{
	// 	setDisp(!disp)
	// 	if(disp){
	// 		setStyle({display:'block'});
	// 	}else{
	// 		setStyle({display:'none'});
	// 	}
	// }

	const nodeList=data;
	return (
		<motion.div
			animate={{ height: categories ? 710 : 0 }}
			transition={{ duration: 0.1 }}
			className="categoriesList_container"
		>
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1 }}
				transition={{ delay: 0.2 }}
			>
				<div className="container-fluid">
					<div className="row">


					<MegaMenu nodeList={nodeList} onNodeClick={val => console.log(val)} />


						{/* <div className="col-4">
							<div className="row">
								{dt.map((prev, i) => {
									return (
										<div onClick={subDiplay}  key={i} className="col-12 mb-4">
											<div className="categoryCard d-flex justify-content-between align-items-center">
												<div className="d-flex align-items-center">
													<img
														src={data[i].img}
														alt="category1"
														className="mr-3"
													/>
													<h4 className="text-dark font-weight-bold">
														{prev.name}
													</h4>
												</div>
												<div className="d-flex justify-content-between align-items-center">
													<h4 className="w-75"> {prev.des} </h4>
													<RiArrowRightSLine
														fontSize="2.5rem"
														color="#067AC2"
													/>
												</div>
											</div>
										</div>
									);
								})}
							</div>
						</div>
				

						<div className="col-4">
							<h2 className="mb-4 font-weight-bold">
								Electroniques et Informatique
							</h2>
							{data2.map((prev, i) => {
								return (
									<div key={i} style={style} className="col-12 px-0 mb-4">
										<div className="categoryCard d-flex justify-content-between align-items-center">
											<div className="d-flex align-items-center">
												<img src={prev.img} alt="category1" className="mr-3" />
												<h4 className="text-dark font-weight-bold">
													{prev.title}
												</h4>
											</div>
											<div className="d-flex justify-content-between align-items-center">
												<h4 className="w-75"> {prev.desc} </h4>
												<RiArrowRightSLine fontSize="2.5rem" color="#067AC2" />
											</div>
										</div>
									</div>
								);
							})}
						</div>


						<div className="col-4"> */}
							{/* <h2 className="mb-4 font-weight-bold">
								Smartphone, Tablettes, Objets...
							</h2>
							{data3.map((prev, i) => {
								return (
									<div key={i} className="col-12 px-0 mb-4">
										<div className="categoryCard d-flex justify-content-between align-items-center">
											<div className="d-flex align-items-center">
												<img src={prev.img} alt="category1" className="mr-3" />
												<h4 className="text-dark font-weight-bold">
													{prev.title}
												</h4>
											</div>
											<div className="d-flex justify-content-between align-items-center">
												<h4 className="w-75"> {prev.desc} </h4>
												<RiArrowRightSLine fontSize="2.5rem" color="#067AC2" />
											</div>
										</div>
									</div>
								);
							})} */}


						{/* </div> */}
				
				</div>
				
				</div>



			</motion.div>
		</motion.div>
	);
};

export default CategoriesList;
