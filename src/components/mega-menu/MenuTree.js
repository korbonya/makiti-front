import React from "react";
import { RiArrowRightSLine } from "react-icons/ri";

const MAX_LEVEL = 10;

const MenuTree = props => {
  let {
    nodeList,
    level,
    maxDepth,
    itemIdSeperator,
    itemId,
    mainContainerRef,
    onNodeSelect
  } = props;
  let megaMenuItemWidth = Math.floor(
    mainContainerRef.current.clientWidth / (maxDepth + 1)
  );
  return (
    <ul
      className={`mega-menu-${level}`}
      style={{
        width: megaMenuItemWidth,
        height: mainContainerRef.current.clientHeight - 2,
        top: mainContainerRef.current.offsetTop,
        left:
          level === 1
            ? mainContainerRef.current.offsetLeft
            : megaMenuItemWidth * (level - 1) +
              mainContainerRef.current.offsetLeft +
              1
      }}
    >
      {nodeList.map((node, i) => (
        <li className="categoryCard d-flex justify-content-between align-items-center" key={`${node.name}-${level}-${i}`}>
          <div
            className="d-flex align-items-center"
            onClick={() =>
              onNodeSelect(
                itemId
                  ? `${itemId}${itemIdSeperator}${node.itemId || node.name}`
                  : node.itemId || node.name
              )
            }
          >
           
              {node.image ? <img className="mr-3" src={process.env.PUBLIC_URL + node.image} alt={"categorie image de "+node.image} /> : null}
         
            <h4 className="text-dark font-weight-bold">{node.name}</h4>
          </div>
                      <div className="d-flex justify-content-between align-items-center">
													<RiArrowRightSLine
														fontSize="2.5rem"
														color="#067AC2"
													/>
						</div>

          {node["sub_categories"] && node["sub_categories"].length > 0 && level <= MAX_LEVEL ? (
            <MenuTree
              level={level + 1}
              nodeList={node["sub_categories"]}
              itemIdSeperator={itemIdSeperator}
              itemId={
                itemId
                  ? `${itemId}${itemIdSeperator}${node.itemId || node.name}`
                  : node.itemId || node.name
              }
              maxDepth={maxDepth}
              onNodeSelect={onNodeSelect}
              mainContainerRef={mainContainerRef}
            />
          ) : node["types-of-articles"] && node["types-of-articles"].length > 0 && level <= MAX_LEVEL ? (
            <MenuTree
              level={level + 1}
              nodeList={node["types-of-articles"]}
              itemIdSeperator={itemIdSeperator}
              itemId={
                itemId
                  ? `${itemId}${itemIdSeperator}${node.itemId || node.name}`
                  : node.itemId || node.name
              }
              maxDepth={maxDepth}
              onNodeSelect={onNodeSelect}
              mainContainerRef={mainContainerRef}
            />
          ) : null}
        </li>
      ))}
    </ul>
  );
};

export default MenuTree;
