import axios from "axios";

const API_URL = "https://nutc5x8n61.execute-api.eu-west-3.amazonaws.com/staging/api/v1";

const register = (phone_number, password) => {
  return axios.post(API_URL + "/users/create", {
    phone_number,
    password,
  })
  .then((response) => {
    return response.data;
  });
};

const codesend=(uid)=>{
  return axios.post(API_URL+"/users/verification-code/send",{uid})
}

const codeconfirm=(uid, code)=>{
  return axios.post(API_URL+"/users/verification-code/confirm",{
    uid, code})
    .then((response)=>{
      return response.data
    })
}

const login = (phone_number, password) => {
  return axios
    .post(API_URL + "/users/login", {
      phone_number,
      password,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem("user");
};


// eslint-disable-next-line import/no-anonymous-default-export
export default {
  register,
  login,
  logout,
  codesend,
  codeconfirm
};