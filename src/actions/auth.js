import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  SET_MESSAGE,
  CODESEND,
  CODECONFIRM,
} from "./types";

import AuthService from "../services/auth.service";

export const register = (phone_number, password) => (dispatch) => {
  return AuthService.register(phone_number, password).then(
    (data) => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: { user: data},
      });

      // dispatch({
      //   type: SET_MESSAGE,
      //   payload: response.data.message,
      // });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REGISTER_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const codesend=(uid)=>(dispatch)=>{
  return AuthService.codesend(uid).then(
    () => {
      dispatch({
        type:CODESEND
      });
      return Promise.resolve()
    }
  )
}

export const codeconfirm=(uid , code) => dispatch => {
  return AuthService.codeconfirm(uid, code).then(
    (data) => {
      dispatch({
        type: CODECONFIRM,
        payload: { user: data },
      });

      return Promise.resolve();
    }
  )
}

export const login = (phone_number, password) => (dispatch) => {
  return AuthService.login(phone_number, password).then(
    (data) => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: { user: data },
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: LOGIN_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const logout = () => (dispatch) => {
  AuthService.logout();

  dispatch({
    type: LOGOUT,
  });
};